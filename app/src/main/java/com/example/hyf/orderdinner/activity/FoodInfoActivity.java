package com.example.hyf.orderdinner.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.CollectStatusBean;
import com.example.hyf.orderdinner.bean.FoodInfoBean;
import com.example.hyf.orderdinner.bean.MyShopCarbean;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FoodInfoActivity extends Activity {
    ImageView food_pic,popwindow_pic;
    TextView food_name,food_price,food_intro,popwindow_name,popwindow_price,popwindow_num;
    public  int food_id,user_id;
    Button bt_shopingcar,bt_popwindow_commit,bt_popwindow_reduce,bt_popwindow_increase;
    SharedPreferences sp;
    String strimg,strfoodname,strprice;
//    public  volatile static int count=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_info);
        initView();
        initData();
//        initEvent(strfoodname,strprice,strimg);
                  initEvent();
    }

    private void initEvent() {
        bt_shopingcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              showPopWindow(strfoodname,strprice,strimg);
            }
        });
    }

//    private void initEvent(final String name, final String price, final String img) {
//        bt_shopingcar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//              showPopWindow(name,price,img);
//            }
//        });
//    }

    private void showPopWindow(final String name, final String price, final String img) {
        View view=LayoutInflater.from(this).inflate(R.layout.popwindow,null);
        final PopupWindow window=new PopupWindow(view,WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        //实例化控件
        popwindow_pic=(ImageView)view.findViewById(R.id.popwindow_pic);
        popwindow_name=(TextView)view.findViewById(R.id.popwindow_foodname);
        popwindow_price=(TextView)view.findViewById(R.id.popwindow_price);
        bt_popwindow_commit=view.findViewById(R.id.popwindow_commit);
        bt_popwindow_increase=view.findViewById(R.id.bt_increase);
        bt_popwindow_reduce=view.findViewById(R.id.bt_reduce);
        popwindow_num=view.findViewById(R.id.popwindow_num);

        popwindow_name.setText(name);
        popwindow_price.setText(price);
        Picasso.get().load(img).into(popwindow_pic);

        bt_popwindow_reduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer count=Integer.valueOf(popwindow_num.getText().toString());
                if(count<=1)
                    return;
                else count--;

                popwindow_num.setText(String.valueOf(count));
            }
        });
        bt_popwindow_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer count=Integer.valueOf(popwindow_num.getText().toString());
                count++;
                popwindow_num.setText(String.valueOf(count));
            }
        });
        bt_popwindow_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num=Integer.valueOf(popwindow_num.getText().toString());
                BaseModel<CollectStatusBean> beanBaseModel=new BaseModel<>();
                beanBaseModel.callEnqueue(beanBaseModel.service.getcommitstatus(user_id, food_id, num), new BaseListener<CollectStatusBean>() {
                    @Override
                    public void onResponse(CollectStatusBean collectStatusBean) {
                        if(collectStatusBean.getSuccess().equals("1")){
                            Toast.makeText(FoodInfoActivity.this, "加入购物车成功", Toast.LENGTH_SHORT).show();
                            window.dismiss();
                        }else{
                            Toast.makeText(FoodInfoActivity.this, "加入购物车状态失败", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(String msg) {
                        Toast.makeText(FoodInfoActivity.this, "加入购物车失败"+msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


           window.setContentView(view);
           window.setOutsideTouchable(false);
           window.setFocusable(true);
        // 实例化一个ColorDrawable颜色为半透明
           window.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//        window.setAnimationStyle(R.style.mypopwindow_anim_style);
           window.showAtLocation(view, Gravity.BOTTOM|Gravity.CENTER_VERTICAL, 0, 0);





    }

    private void initData() {
        Intent intent=getIntent();
        food_id=intent.getIntExtra("food_id",0);
        sp=getSharedPreferences("OrderDinner",0);
        SharedPreferences.Editor editor=sp.edit();
        editor.putInt("food_id",food_id).putInt("status",1).commit();
        user_id=sp.getInt("USERID",0000000000000);

        BaseModel<FoodInfoBean> model=new BaseModel<>();
        model.callEnqueue(model.service.getFoodInfo(Integer.valueOf(food_id)), new BaseListener<FoodInfoBean>() {
            @Override
            public void onResponse(FoodInfoBean foodInfoBean) {
                strfoodname=foodInfoBean.getFoodname();
                strprice=String.valueOf(foodInfoBean.getPrice());
                strimg=URL.BaseURL+foodInfoBean.getPic();

                food_name.setText(strfoodname);
                food_price.setText(strprice);
                food_intro.setText(foodInfoBean.getIntro());
                Picasso.get().load(strimg).into(food_pic);
            }

            @Override
            public void onFail(String msg) {
                Toast.makeText(FoodInfoActivity.this, "获取菜谱详情失败", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        food_pic=(ImageView)findViewById(R.id.foodinfo_pic);
        food_intro=(TextView)findViewById(R.id.foodinfo_instro);
        food_name=(TextView)findViewById(R.id.foodinfo_name);
        food_price=(TextView)findViewById(R.id.foodinfo_price);
        bt_shopingcar=(Button)findViewById(R.id.bt_shoppingcar);

//                Toast.makeText(this, food_id, Toast.LENGTH_SHORT).show();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        count=1;
    }
}
