package com.example.hyf.orderdinner.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.fragments.CollectFragment;
import com.example.hyf.orderdinner.fragments.SearchFragment;
import com.example.hyf.orderdinner.fragments.ShopFragment;
import com.example.hyf.orderdinner.fragments.UserInfoFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener,ViewPager.OnPageChangeListener{
    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private FragmentPagerAdapter adapter;
    private List<Fragment> fragmentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        viewPager=(ViewPager) findViewById(R.id.viewPager);
        bottomNavigationView=(BottomNavigationView)findViewById(R.id.bottomNavigationView);
        initfragment();

        adapter=new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        };
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(fragmentList.size());//设置viewpager缓存页数量
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.nav_shop);
    }

    private void initfragment() {
        fragmentList=new ArrayList<>();
        fragmentList.add(new ShopFragment());
        fragmentList.add(new CollectFragment());
        fragmentList.add(new SearchFragment());
        fragmentList.add(new UserInfoFragment());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=0;
        switch (item.getItemId()){
            case R.id.nav_shop:
                id=0;
                break;
            case R.id. nav_collection:
                id=1;
                break;
            case R.id.nav_search:
                id=2;
                break;
            case R.id.nav_user:
                id=3;
                break;

        }
        viewPager.setCurrentItem(id);

        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
      int id=0;
      switch (position){
          case 0:
              id=R.id.nav_shop;
              break;
          case 1:
              id=R.id.nav_collection;
              break;
          case 2:
              id=R.id.nav_search;
              break;
          case 3:
              id=R.id.nav_user;
              break;

      }
      bottomNavigationView.setSelectedItemId(id);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
