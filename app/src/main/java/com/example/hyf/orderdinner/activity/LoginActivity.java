package com.example.hyf.orderdinner.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.bean.ResultBean;
import com.example.hyf.orderdinner.model.LoginModel;

public class LoginActivity extends AppCompatActivity {
    //定义SharedPreferences的名称
    Button bt_login,bt_regist;
    EditText et_username,et_password;
    SharedPreferences sp;
    public static int user_id;
    // 定义一个变量，来标识是否退出
    private static boolean isExit = false;

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };
    private BaseListener<ResultBean> listener=new BaseListener<ResultBean>() {
        @Override
        public void onResponse(ResultBean resultBean) {
            if(resultBean.getUserid()==0){
                Toast.makeText(LoginActivity.this,"登录失败",Toast.LENGTH_SHORT).show();
                return;
            }
            else {
                   user_id=resultBean.getUserid();
                Toast.makeText(LoginActivity.this, "userid:"+user_id, Toast.LENGTH_SHORT).show();
                  sp=getSharedPreferences("OrderDinner",0);
                SharedPreferences.Editor editor=sp.edit();
                        editor.putInt("USERID",user_id)
                                .commit();
                Intent intent=new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        public void onFail(String fstr) {
            Toast.makeText(LoginActivity.this,"失败！！！！",Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onStart() {
        loadSharedPreferences();
        super.onStart();

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        intview();
        bt_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,RegistActivity.class);
                startActivityForResult(intent,1);
            }
        });
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = et_username.getText().toString();
                String userpass = et_password.getText().toString();
                if(userpass.equals("")||username.equals("")){
                    Toast.makeText(LoginActivity.this,"请输入正确的账号或密码",Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
//                    Toast.makeText(LoginActivity.this,et_username.getText().toString()+et_password.getText().toString(),Toast.LENGTH_SHORT).show();
                    new LoginModel().getlogin(username,userpass,listener);


                }


            }
        });
    }

//    // 用来计算返回键的点击间隔时间
//    private long exitTime = 0;
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK
//                && event.getAction() == KeyEvent.ACTION_DOWN) {
//            if ((System.currentTimeMillis() - exitTime) > 2000) {
//                //弹出提示，可以有多种方式
//                Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
//                exitTime = System.currentTimeMillis();
//            } else {
//                finish();
//            }
//            return true;
//        }
//
//        return super.onKeyDown(keyCode, event);

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case 1:{
                if(resultCode==RESULT_OK){
                    et_username.setText(data.getStringExtra("usernmae_data"));
                }
                break;
            }
            default:break;
        }
    }

    private void intview() {
        bt_login=(Button)findViewById(R.id.bt_login);
        bt_regist=(Button)findViewById(R.id.bt_regist);
        et_password=(EditText)findViewById(R.id.editText6);
        et_username=(EditText)findViewById(R.id.et_username);
    }

    @Override
    protected void onStop() {

        saveSharedPreferences();

        super.onStop();
        


    }


    private void loadSharedPreferences() {
        //获取一个文件名为test、权限为private的xml文件的SharedPreferences对象
        SharedPreferences sharedPreferences=getSharedPreferences("OrderDinner",0);
        //得到SharedPreferences.Editor对象，并保存数据到该对象中
//                SharedPreferences.Editor editor=sharedPreferences.edit();
        String name= sharedPreferences.getString("NAME","");
        String pass=sharedPreferences.getString("PASSWORD","");
        et_username.setText(name);
        et_password.setText(pass);
    }
    private void saveSharedPreferences() {
        SharedPreferences sharedPreferences=getSharedPreferences("OrderDinner",0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("NAME",et_username.getText().toString())
                .putString("PASSWORD",et_password.getText().toString())
                        .putInt("status",0)//判断在店铺页面（0）还是菜品页面（1）
                        .commit();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            finish();
            System.exit(0);
        }
    }
}
