package com.example.hyf.orderdinner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.bean.RegisterBean;
import com.example.hyf.orderdinner.model.RegistModel;

public class RegistActivity extends AppCompatActivity {
    EditText regist_username,regist_password,regist_confirm,regist_telnumber,regist_address,regist_comment;
    Button bt_confirm;
    String username;
    BaseListener<RegisterBean> listener=new BaseListener<RegisterBean>() {
        @Override
        public void onResponse(RegisterBean registerBean) {
            if(registerBean.getSuccess().equals("0")){
                Toast.makeText(RegistActivity.this,"注册失败!!!",Toast.LENGTH_SHORT).show();
                return;
            }
            else{
                Toast.makeText(RegistActivity.this,"注册成功",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent();
                intent.putExtra("username_data",username);
                setResult(RESULT_OK,intent);
                finish();
            }
        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(RegistActivity.this,msg,Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        initview();


        bt_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    username=regist_username.getText().toString();
                    String userpass=regist_password.getText().toString();
                    String usercfpass=regist_confirm.getText().toString();
                    String mobilenum=regist_telnumber.getText().toString();
                    String address=regist_address.getText().toString();
                    String comment=regist_comment.getText().toString();

//                    Toast.makeText(RegistActivity.this,userpass+"..."+usercfpass,Toast.LENGTH_SHORT).show();
                    if(username.equals("")){
                        Toast.makeText(RegistActivity.this,"请输入账号名称",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(!(userpass.equals(usercfpass))){
                        Toast.makeText(RegistActivity.this,"两次输入密码不一致",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        new RegistModel().register(username,userpass,mobilenum,address,comment,listener);

                    }

                }
            });



    }

    private void initview() {
        regist_password=(EditText)findViewById(R.id.regist_pass);
        regist_username=(EditText)findViewById(R.id.regsit_username);
        regist_confirm=(EditText) findViewById(R.id.regist_confirpass);
        regist_telnumber=(EditText)findViewById(R.id.regist_telnumbe);
        regist_address=(EditText)findViewById(R.id.regsit_address);
        regist_comment=(EditText)findViewById(R.id.regist_comment);
        bt_confirm=(Button)findViewById(R.id.bt_confirm);
    }



}
