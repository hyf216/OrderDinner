package com.example.hyf.orderdinner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.adapter.SearchAdapter;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.SearchResultBean;

import java.util.List;

public class SearchRusltActivity extends AppCompatActivity {
   RecyclerView recyclerView;
   SearchAdapter adapter;
   List<SearchResultBean> listdata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_ruslt);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        recyclerView=(RecyclerView)findViewById(R.id.rv_search);
        adapter=new SearchAdapter();
        recyclerView.setAdapter(adapter);
        LinearLayoutManager manager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);
    }
    private void initData() {
        Intent intent=getIntent();
        String search_info = intent.getStringExtra("search_info");
        BaseModel<List<SearchResultBean>> model=new BaseModel<>();
        model.callEnqueue(model.service.getSearch(search_info), new BaseListener<List<SearchResultBean>>() {
            @Override
            public void onResponse(List<SearchResultBean> searchResultBeans) {
                if(searchResultBeans!=null){
                    listdata=searchResultBeans;
                    adapter.setData(searchResultBeans);
//                                  for(int i=0;i<searchResultBeans.size();i++){
//                                      getfoodid.add(searchResultBeans.get(i).getFood_id());
//                                  }

                }else {
                    Toast.makeText(getApplicationContext(), "Response为空", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail(String msg) {
                Toast.makeText(getApplicationContext(), "SearchFragment:"+msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void initEvent() {
         adapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
             @Override
             public void setItem(View view, int position) {
                 Intent intent=new Intent(SearchRusltActivity.this,FoodInfoActivity.class);
                 intent.putExtra("food_id",listdata.get(position).getFood_id());
                 startActivity(intent);
             }
         });
    }
}
