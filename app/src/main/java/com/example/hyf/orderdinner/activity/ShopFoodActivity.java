package com.example.hyf.orderdinner.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Adapter;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.adapter.ListFoodAdapter;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.base.TopBar;
import com.example.hyf.orderdinner.bean.ShopFoodBean;

import java.util.List;

public class ShopFoodActivity extends AppCompatActivity {
    List<ShopFoodBean> list;
    ListFoodAdapter adapter;
    RecyclerView recyclerView;
    SharedPreferences sp;
    public  int shop_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopfood);
        initView();
        initData();
        inititen();

    }

    private void initView() {
        recyclerView=(RecyclerView)findViewById(R.id.rv_shopfood);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter=new ListFoodAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    }
    private void initData() {
        Intent intent=getIntent();
        shop_id = intent.getIntExtra("shop_id",0);

        sp=getSharedPreferences("OrderDinner",0);
        SharedPreferences.Editor editor=sp.edit();
        editor.putInt("shop_id",shop_id).putInt("status",0).commit();


//        Toast.makeText(ShopFoodActivity.this,getsho_pid,Toast.LENGTH_SHORT).show();
        BaseModel<List<ShopFoodBean>> model=new BaseModel<>();
        model.callEnqueue(model.service.getShopFood(shop_id), new BaseListener<List<ShopFoodBean>>() {
            @Override
            public void onResponse(List<ShopFoodBean> shopFoodBeans) {
                   list=shopFoodBeans;
                   if(list==null){
                       Toast.makeText(ShopFoodActivity.this,"数据获取失败",Toast.LENGTH_SHORT).show();
                   }else {
                       adapter.setData(list);
                   }
            }

            @Override
            public void onFail(String msg) {
                 Toast.makeText(ShopFoodActivity.this,msg+""+"ShopFoodActivity",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void inititen() {
        adapter.setOnItemClickListener(new ListFoodAdapter.OnItemClickListener() {
            @Override
            public void setItem(View view, int position) {
                Intent intent=new Intent(ShopFoodActivity.this,FoodInfoActivity.class);
                intent.putExtra("food_id",list.get(position).getFood_id());
                sp=getSharedPreferences("OrderDinner",0);
                SharedPreferences.Editor editor=sp.edit();
                editor.putInt("food_id",list.get(position).getFood_id()).putInt("status",1).commit();
//                Toast.makeText(ShopFoodActivity.this, String.valueOf(list.get(position).getFood_id()), Toast.LENGTH_SHORT).show();
                 startActivity(intent);
            }
        });
    }
}
