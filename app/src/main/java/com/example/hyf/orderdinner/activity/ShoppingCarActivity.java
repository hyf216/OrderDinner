package com.example.hyf.orderdinner.activity;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.adapter.MyShopCarAdapter;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.MyShopCarbean;

import java.util.List;

public class ShoppingCarActivity extends AppCompatActivity {
     MyShopCarAdapter adapter;
     RecyclerView recyclerView;
     SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_car);
        initView();
        initData();
    }

    private void initView() {
       recyclerView=(RecyclerView)findViewById(R.id.rv_shopcar);
       adapter=new MyShopCarAdapter();
       recyclerView.setAdapter(adapter);
        LinearLayoutManager manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
    }

    private void initData() {
        sp=getSharedPreferences("OrderDinner",0);
        int userid=sp.getInt("USERID",0000000000);
        BaseModel<List<MyShopCarbean>> model=new BaseModel<>();
        model.callEnqueue(model.service.getMyShopCarInfo(userid), new BaseListener<List<MyShopCarbean>>() {
            @Override
            public void onResponse(List<MyShopCarbean> myShopCarbeans) {
                if(myShopCarbeans!=null&&myShopCarbeans.size()!=0){
                    adapter.setData(myShopCarbeans);
                }
                else Toast.makeText(ShoppingCarActivity.this, "ShoppingcarActivity：获取响应失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail(String msg) {
                Toast.makeText(ShoppingCarActivity.this, "获取信息失败"+msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
