package com.example.hyf.orderdinner.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.base.OnMainListener;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.RegisterBean;
import com.example.hyf.orderdinner.bean.UserInfoBean;
import com.example.hyf.orderdinner.fragments.UserInfoFragment;
import com.squareup.picasso.Picasso;

public class UpdateInfoActivity extends AppCompatActivity implements OnMainListener {
      TextView update_name,update_pw,update_tel,update_address;
      Button save;
      int user_id;
//      private  OnMainListener onMainListener;
      String getupdate_name,getupdate_pw,getupdate_tel,getupdate_address;
      SharedPreferences sharedPreferences;
      int mflag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info);
        initView();
        showData();
        initData();

    }

    private void showData() {
        BaseModel<UserInfoBean> model=new BaseModel<>();
        model.callEnqueue(model.service.getUserInfo(user_id), new BaseListener<UserInfoBean>() {
            @Override
            public void onResponse(UserInfoBean userInfoBean) {
                update_name.setText(userInfoBean.getUsername());
                update_tel.setText(userInfoBean.getMobilenum());
                update_address.setText(userInfoBean.getAddress());
            }

            @Override
            public void onFail(String msg) {
                Toast.makeText(UpdateInfoActivity.this, "UserInfoFragment:"+msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

//    @Override
//    public void onAttachFragment(Fragment fragment) {
//        try{
//            onMainListener = (OnMainListener)fragment;
//        }catch (Exception e){
//            throw new ClassCastException(this.toString() + " must implementOnMainListener");
//        }
//        super.onAttachFragment(fragment);
//    }

    private void initData() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getupdate_name=update_name.getText().toString();
                getupdate_pw=update_pw.getText().toString();
                getupdate_tel=update_tel.getText().toString();
                getupdate_address=update_address.getText().toString();
                if(getupdate_name.equals("")||getupdate_pw.equals("")||getupdate_tel.equals("")||getupdate_address.equals("")){
                    Toast.makeText(UpdateInfoActivity.this, "某项信息为空", Toast.LENGTH_SHORT).show();
                }else {
                    BaseModel<RegisterBean> model=new BaseModel<>();
                    model.callEnqueue(model.service.getupdateUserinfo(user_id,getupdate_name,getupdate_pw,getupdate_tel,getupdate_address),new BaseListener<RegisterBean>() {
                        @Override
                        public void onResponse(RegisterBean registerBean) {
                            if(registerBean.getSuccess().equals("1")){
                                Toast.makeText(UpdateInfoActivity.this, "更改成功", Toast.LENGTH_SHORT).show();
//                                Intent intent=new Intent(getApplicationContext(),UserInfoFragment.class);
////                                intent.putExtra("");
//                                startActivity(intent);
                                mflag=1;
                                finish();
                            }
                            else {
                                Toast.makeText(UpdateInfoActivity.this, "更改失败", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFail(String msg) {
                            Toast.makeText(UpdateInfoActivity.this, "更改失败:"+msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

        });


    }
//    public void onMainAction(TextView t1,TextView t2){
//                  t1.setText(getupdate_name);
//                  t2.setText(getupdate_pw);
//
//    }
    private void initView() {
        update_name=(TextView)findViewById(R.id.update_username);
        update_pw=(TextView)findViewById(R.id.update_psw);
        update_tel=(TextView)findViewById(R.id.update_tel);
        update_address=(TextView)findViewById(R.id.update_address);
        save=(Button)findViewById(R.id.update_save);
        sharedPreferences=getSharedPreferences("OrderDinner",0);
        user_id=sharedPreferences.getInt("USERID",0000000);
        update_pw.setText(sharedPreferences.getString("PASSWORD","000000000"));
    }


    @Override
    public void onMainAction(int flag) {
        flag=mflag;
    }
}
