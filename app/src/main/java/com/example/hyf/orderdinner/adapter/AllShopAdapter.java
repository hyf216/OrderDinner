package com.example.hyf.orderdinner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.bean.AllShopBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllShopAdapter extends RecyclerView.Adapter<AllShopAdapter.ShopViewHolder>{
    List<AllShopBean> listshop;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
            void setItem(View view,int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }




    public class ShopViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView shopname,shop_instro,shop_laocation,shop_level;
        public ShopViewHolder(View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.imageView3);
            shopname=(TextView) itemView.findViewById(R.id.shop_name);
            shop_instro=(TextView)itemView.findViewById(R.id.shop_instro);
            shop_laocation=(TextView)itemView.findViewById(R.id.shop_location);
            shop_level=(TextView)itemView.findViewById(R.id.shop_level);
        }
    }

    public void setData(List<AllShopBean> list_shop){
        this.listshop = list_shop;
        notifyDataSetChanged();
    }
    @Override
    public ShopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop,parent,false);
        ShopViewHolder shopViewHolder=new ShopViewHolder(view);

        return shopViewHolder;

    }

    @Override
    public void onBindViewHolder(ShopViewHolder holder, final int position) {
        AllShopBean allShopBean=listshop.get(position);
        Picasso.get().load(URL.BaseURL + allShopBean.getPic()).into(holder.imageView);
        holder.shopname.setText(allShopBean.getShopname());
        holder.shop_instro.setText(allShopBean.getIntro());
        holder.shop_laocation.setText(allShopBean.getAddress());
        holder.shop_level.setText(allShopBean.getLevel()+"");

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.setItem(v, position);
                }
            });
        }



    }

    @Override
    public int getItemCount() {
        return listshop==null?0:listshop.size();
    }

}
