package com.example.hyf.orderdinner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.CollectListBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CollectFoodAdapter extends RecyclerView.Adapter<CollectFoodAdapter.CollectFoodViewHolder> {

    List<CollectListBean> collectListBeans;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void setItem(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void setData (List<CollectListBean> collectListBean){
        this.collectListBeans = collectListBean;
        notifyDataSetChanged();
    }
    public  class CollectFoodViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_food;
        TextView tv_foodname,tv_foodprice;
        public CollectFoodViewHolder(View itemView) {
            super(itemView);
            iv_food=(ImageView)itemView.findViewById(R.id.collect_food_img);
            tv_foodname=(TextView)itemView.findViewById(R.id.tv_item_foodname);
            tv_foodprice=(TextView)itemView.findViewById(R.id.tv_item_foodprice);
        }
    }
    @Override
    public CollectFoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collectfood,parent,false);
        CollectFoodViewHolder holder=new CollectFoodViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CollectFoodViewHolder holder, final int position) {
      CollectListBean bean= collectListBeans.get(position);
        Picasso.get().load(URL.BaseURL+bean.getPic()).into(holder.iv_food);
        holder.tv_foodname.setText(bean.getFoodname());
        holder.tv_foodprice.setText(String.valueOf(bean.getPrice()));

        if(onItemClickListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.setItem(view,position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return collectListBeans ==null?0: collectListBeans.size();
    }
}
