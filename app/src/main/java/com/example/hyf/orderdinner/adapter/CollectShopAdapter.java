package com.example.hyf.orderdinner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.CollectListBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CollectShopAdapter extends RecyclerView.Adapter<CollectShopAdapter.CollectShopViewHolder> {
    List<CollectListBean> collectShopDatatBeans;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void setItem(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public CollectShopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collectshop, parent, false);
        CollectShopViewHolder holder = new CollectShopViewHolder(view);
        return holder;
    }
    public class CollectShopViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_shop;
        TextView tv_shopname, tv_shopaddress;

        public CollectShopViewHolder(View itemView) {
            super(itemView);
            iv_shop = (ImageView) itemView.findViewById(R.id.item_shopimg);
            tv_shopname = (TextView) itemView.findViewById(R.id.tv_item_shopname);
            tv_shopaddress = (TextView) itemView.findViewById(R.id.tv_item_shopaddress);
        }

    }

    @Override
    public void onBindViewHolder(CollectShopViewHolder holder, final int position) {
        CollectListBean bean = collectShopDatatBeans.get(position);
        Picasso.get().load(URL.BaseURL+bean.getPic()).into(holder.iv_shop);
        holder.tv_shopname.setText(bean.getShopname());
        holder.tv_shopaddress.setText(bean.getAddress());

        if(onItemClickListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.setItem(view,position);
                }
            });
        }
    }
     public void setData(List<CollectListBean> listBeans){
        this.collectShopDatatBeans =listBeans;
        notifyDataSetChanged();
     }

    @Override
    public int getItemCount() {
        return collectShopDatatBeans == null ? 0 : collectShopDatatBeans.size();
    }



}