package com.example.hyf.orderdinner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.ShopFoodBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListFoodAdapter extends RecyclerView.Adapter<ListFoodAdapter.ListFoodViewHolder> {
    List<ShopFoodBean> listfoodbean;
    private OnItemClickListener onItemClickListener;
    public interface OnItemClickListener{
        void setItem(View view,int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ListFoodViewHolder extends RecyclerView.ViewHolder{
        ImageView food_pic;
        TextView food_name,food_instr,food_price;
        public ListFoodViewHolder(View itemView) {
            super(itemView);
            food_pic=(ImageView)itemView.findViewById(R.id.food_pic);
            food_instr=(TextView)itemView.findViewById(R.id.food_instr);
            food_name=(TextView)itemView.findViewById(R.id.food_name);
            food_price=(TextView)itemView.findViewById(R.id.food_price);

        }
    }
    public void setData(List<ShopFoodBean> foodlist){
        this.listfoodbean=foodlist;
        notifyDataSetChanged();
    }
    @Override
    public ListFoodAdapter.ListFoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shopfood,parent,false);
        ListFoodViewHolder holder=new ListFoodViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ListFoodAdapter.ListFoodViewHolder holder, final int position) {
        ShopFoodBean foodListBean=listfoodbean.get(position);
        Picasso.get().load(URL.BaseURL+foodListBean.getPic()).into(holder.food_pic);
        holder.food_instr.setText(foodListBean.getIntro());
        holder.food_name.setText(foodListBean.getFoodname());
        holder.food_price.setText(String.valueOf(foodListBean.getPrice()));
        if(onItemClickListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.setItem(view,position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listfoodbean==null?0:listfoodbean.size();
    }
}
