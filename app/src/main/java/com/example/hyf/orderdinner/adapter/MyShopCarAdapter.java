package com.example.hyf.orderdinner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.MyShopCarbean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyShopCarAdapter extends RecyclerView.Adapter<MyShopCarAdapter.MyShopCarViewHolder> {

    List<MyShopCarbean> listbeans;

    @Override
    public MyShopCarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shopcar,parent,false);
        MyShopCarViewHolder viewHolder=new MyShopCarViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyShopCarViewHolder holder, int position) {
        MyShopCarbean bean=listbeans.get(position);
        Picasso.get().load(URL.BaseURL+bean.getPic()).into(holder.shopcar_pic);
        holder.tv_shopcar_foodname.setText(bean.getFoodname());
        double num=bean.getNum();
         double price=bean.getPrice();
        holder.tv_shopcar_totalprice.setText(String.valueOf(num*price));
        holder.tv_shopcar_num.setText(String.valueOf(bean.getNum()));
    }
     public void setData(List<MyShopCarbean> list){
        this.listbeans=list;
        notifyDataSetChanged();
     }
    @Override
    public int getItemCount() {
        return listbeans==null?0:listbeans.size();
    }

    public class MyShopCarViewHolder extends RecyclerView.ViewHolder{
        ImageView shopcar_pic;
        TextView tv_shopcar_foodname,tv_shopcar_totalprice,tv_shopcar_num;

        public MyShopCarViewHolder(View itemView) {
            super(itemView);
            shopcar_pic=itemView.findViewById(R.id.shopcar_pic);
            tv_shopcar_foodname=itemView.findViewById(R.id.shopcar_foodname);
            tv_shopcar_totalprice=itemView.findViewById(R.id.shopcar_totalprice);
            tv_shopcar_num=itemView.findViewById(R.id.item_shopcar_num);
        }
    }
}
