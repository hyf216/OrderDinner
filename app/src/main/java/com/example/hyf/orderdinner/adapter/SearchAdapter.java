package com.example.hyf.orderdinner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.SearchResultBean;
import com.squareup.picasso.Picasso;

import java.util.List;



public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    List<SearchResultBean> listbeans;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void setItem(View view,int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }
    public void setData(List<SearchResultBean> resultBeans){
        this.listbeans=resultBeans;
        notifyDataSetChanged();
    }
    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search,parent,false);
        SearchViewHolder holder=new SearchViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {
         SearchResultBean bean=listbeans.get(position);
        Picasso.get().load(URL.BaseURL+bean.getPic()).into(holder.iv_search_pic);
        holder.tv_search_name.setText(bean.getFoodname());
        holder.tv_search_intro.setText(bean.getIntro());
        holder.tv_search_price.setText(String.valueOf(bean.getPrice()));

        if(onItemClickListener!=null){
           holder.itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   onItemClickListener.setItem(view,position);
               }
           });
        }
    }

    @Override
    public int getItemCount() {
        return listbeans==null?0:listbeans.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_search_pic;
        TextView tv_search_name,tv_search_intro,tv_search_price;
        public SearchViewHolder(View itemView) {
            super(itemView);
            iv_search_pic=itemView.findViewById(R.id.iv_search_pic);
            tv_search_intro=itemView.findViewById(R.id.tv_search_intro);
            tv_search_name=itemView.findViewById(R.id.tv_search_foodname);
            tv_search_price=itemView.findViewById(R.id.tv_search_price);
        }
    }


}
