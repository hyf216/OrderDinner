package com.example.hyf.orderdinner.base;

public interface BaseListener <T>{
    void onResponse(T t);
    void onFail(String msg);

}
