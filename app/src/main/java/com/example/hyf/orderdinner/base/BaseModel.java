package com.example.hyf.orderdinner.base;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseModel<T> {
    public Retrofit retrofit;
    public Service service;
    public Call<T> call;


    public BaseModel() {
        retrofit=new Retrofit.Builder()
                .baseUrl(URL.BaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(Service.class);
    }

    public void callEnqueue(Call<T> call,final BaseListener<T> listner){
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.body()!=null&&response.isSuccessful()) {
                    listner.onResponse(response.body());
                }else
                    listner.onFail(response.message()+response.code());
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                listner.onFail(t.getMessage());
            }
        });
    }
}
