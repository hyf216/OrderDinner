package com.example.hyf.orderdinner.base;




import com.example.hyf.orderdinner.bean.AllShopBean;
import com.example.hyf.orderdinner.bean.CollectListBean;
import com.example.hyf.orderdinner.bean.CollectStatusBean;
import com.example.hyf.orderdinner.bean.FoodInfoBean;
import com.example.hyf.orderdinner.bean.IsCollectBean;
import com.example.hyf.orderdinner.bean.MyShopCarbean;
import com.example.hyf.orderdinner.bean.RegisterBean;
import com.example.hyf.orderdinner.bean.ResultBean;
import com.example.hyf.orderdinner.bean.SearchResultBean;
import com.example.hyf.orderdinner.bean.ShopFoodBean;
import com.example.hyf.orderdinner.bean.UserBean;
import com.example.hyf.orderdinner.bean.UserInfoBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service {
    //登录接口
    @GET("userLogin.do")
    Call<ResultBean> getlogin(@Query("username") String username, @Query("userpass") String userpass);

    //注册
    @GET("userRegister.do")
    Call<RegisterBean> register(@Query("username") String username,
                                @Query("userpass") String userpass,
                                @Query("mobilenum") String mobilenum,
                                @Query("address") String address,
                                @Query("comment")  String comment);
    //获取所有店铺
    @GET("getAllShops.do")
    Call<List<AllShopBean>> getAllShop();

    //获取当前店铺的信息
    @GET("getFoodByShop.do")
    Call<List<ShopFoodBean>> getShopFood(@Query("shop_id") Integer shop_id);

    //获取当前菜谱信息
    @GET("getFoodById.do")
    Call<FoodInfoBean>  getFoodInfo(@Query("food_id") Integer food_id);

    //直接购买
    @GET("insertOrder.do")
    Call<UserBean> doBuy(@Query("user_id") int user_id,
                         @Query("food_id") int food_id,
                         @Query("num") int num,
                         @Query("sum") double sum,
                         @Query("suggesttime") String suggesttime,
                         @Query("address ") String address);

    //收藏/取消收藏店铺
    @GET("userCollectShop.do")
    Call<CollectStatusBean> doCollectShop(@Query("user_id") int user_id,
                                          @Query("shop_id") int shop_id);
    //收藏/取消收藏菜谱
    @GET("userCollectFood.do ")
    Call<CollectStatusBean> doCollectFood(@Query("user_id") int user_id,
                                 @Query("food_id") int food_id);


    //判断是否收藏
    @GET("isCollected.do")
    Call<IsCollectBean> isCollected(@Query("user_id") int user_id,
                                    @Query("shop_food_id") int shop_food_id,
                                    @Query("flag") int flag);


    //获取收藏列表
    @GET("getAllUserCollection.do")
    Call<List<CollectListBean>> getCollectList(@Query("user_id") int user_id,
                                               @Query("flag") int flag);

    //搜索
    @GET("getFoodBySearch.do")
    Call<List<SearchResultBean>> getSearch(@Query("search") String search);

    //获取用用户信息
    @GET("getUserById.do")
    Call<UserInfoBean> getUserInfo(@Query("user_id") Integer user_id);

    //修改用户信息
    @GET("updateUserById.do")
    Call<RegisterBean> getupdateUserinfo(@Query("user_id") int user_id,
                                         @Query("username") String username,
                                         @Query("userpass") String userpass,
                                         @Query("mobilenum") String mobilenum,
                                         @Query("address") String address);
    //提交购物车
    @GET("addCart.do")
    Call<CollectStatusBean> getcommitstatus(@Query("user_id") int user_id,
                                            @Query("food_id") int food_id,
                                            @Query("num") int num);
    //当前购物车
    @GET("getMyCartByUser.do")
    Call<List<MyShopCarbean>> getMyShopCarInfo(@Query("user_id") int user_id);
}
