package com.example.hyf.orderdinner.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.activity.FoodInfoActivity;
import com.example.hyf.orderdinner.activity.LoginActivity;
import com.example.hyf.orderdinner.activity.ShopFoodActivity;
import com.example.hyf.orderdinner.bean.CollectStatusBean;
import com.example.hyf.orderdinner.bean.IsCollectBean;


public class TopBar extends LinearLayout implements View.OnClickListener{


    private Context context;
    ImageView iv_back, nocollect_img,collect_pic;
    TextView  topbar_name;
    SharedPreferences sharedPreferences=getContext().getSharedPreferences("OrderDinner",0);

    boolean iscollectshop;
    boolean iscollectfood;
    int userId=sharedPreferences.getInt("USERID",2222222);
    int foodid=sharedPreferences.getInt("food_id",0000);
    int shopid=sharedPreferences.getInt("shop_id",1111);
    int status =sharedPreferences.getInt("status",2);
    public TopBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        setWillNotDraw(false);
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater !=null;
        layoutInflater.inflate(R.layout.topbar,this);
        initView();
        intitView(attrs);
        if(status==0){
            docollect(userId,shopid,status);
        }else if(status==1) {
            docollect(userId,foodid,status);
        }
    }
    private void initView() {
        nocollect_img=(ImageView)findViewById(R.id.nocollect_tbimg);
        collect_pic=(ImageView)findViewById(R.id.collect_tbimg);
        topbar_name=(TextView)findViewById(R.id.topbar_name);
        iv_back=(ImageView)findViewById(R.id.back_tbimg);
        iv_back.setOnClickListener(this);
        collect_pic.setOnClickListener(this);
        nocollect_img.setOnClickListener(this);
    }

    private void intitView(AttributeSet attrs) {
        TypedArray a=null;
        try {
            a=getContext().obtainStyledAttributes(attrs,R.styleable.TopBar);
            int n=a.getIndexCount();
            for (int i=0;i<n;i++){
                int attr=a.getIndex(i);
                if(attr==R.styleable.TopBar_topbar_name){
                    setText(topbar_name,a.getString(attr));
                }else if(attr==R.styleable.TopBar_topbar_nameVisibility){
                    setVisibility(topbar_name,0);
                }else if(attr==R.styleable.TopBar_tb_collectVisibility){
                    setVisibility(collect_pic,a.getInt(attr,0));
                }else if(attr==R.styleable.TopBar_tb_nocollectVisibility){
                    setVisibility(nocollect_img,a.getInt(attr,0));
                }else if(attr==R.styleable.TopBar_backVisibility){
                    setVisibility(iv_back,a.getInt(attr,0));
                }



            }
        }finally {
            if (null != a) {
                a.recycle();
            }
        }
    }
    private void setText(TextView tv, CharSequence text) {
        tv.setText(text);
    }
    private void setVisibility(View view, int visibility) {
        switch (visibility) {
            case 0:
                view.setVisibility(View.VISIBLE);
                break;
            case 1:
                view.setVisibility(View.INVISIBLE);
                break;
            case 2:
                view.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_tbimg:
                ((Activity)context).finish();
                break;
            case R.id.nocollect_tbimg:
                switch (status){
                    case 0:
                        if(iscollectshop==false){
                           collectShop(shopid);
                           if(iscollectshop==false){
                               setVisibility(collect_pic,0);
                               setVisibility(nocollect_img,2);
                               Toast.makeText(context, "收藏店铺成功", Toast.LENGTH_SHORT).show();
                           }else {
                               Toast.makeText(context, "收藏：collectshop()返回的iscollectshop为true", Toast.LENGTH_SHORT).show();
                           }
                        }else {
                            Toast.makeText(context, "收藏：iscollectshop值为true", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        if(iscollectfood==false){
                            collectFood(foodid);
                            if(iscollectfood==false){
                                setVisibility(collect_pic,0);
                                setVisibility(nocollect_img,2);
                                Toast.makeText(context, "收藏菜品成功", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(context, "收藏：collectfood()返回的iscollectfood为true", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, "收藏：iscollectfood值为true", Toast.LENGTH_SHORT).show();
                        }
                        break;
                        default:break;
                }
                break;
            case R.id.collect_tbimg:
                switch (status){
                    case 0:
                        if(iscollectshop==true){
                            collectShop(shopid);
                            if(iscollectshop==true){
                                setVisibility(nocollect_img,0);
                                setVisibility(collect_pic,2);
                                Toast.makeText(context, "取消收藏店铺成功", Toast.LENGTH_SHORT).show();
                                iscollectshop=false;
                            }else {
                                Toast.makeText(context, "取消收藏店铺：collectshop返回的iscollectshop值为false", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(context, "iscollectshop值为false", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        if(iscollectfood==true){
                            collectFood(foodid);
                            if(iscollectfood==true){
                                setVisibility(nocollect_img,0);
                                setVisibility(collect_pic,2);
                                Toast.makeText(context, "取消收藏菜品成功", Toast.LENGTH_SHORT).show();
                                iscollectfood=false;
                            }
                            else {
                                Toast.makeText(context, "取消收藏菜品：collectfood返回iscollectfood为false", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, "iscollectfood值为false", Toast.LENGTH_SHORT).show();
                        }
                        break;
                        default:break;
                }
                break;
            default:break;
        }
    }

    private Boolean collectFood(int food_id) {
        BaseModel<CollectStatusBean> model=new BaseModel<>();
        model.callEnqueue(model.service.doCollectFood(userId,food_id), new BaseListener<CollectStatusBean>() {
            @Override
            public void onResponse(CollectStatusBean collectStatusBean) {
                if(collectStatusBean.getSuccess().equals("1")){
                    iscollectfood=true;
                }else {
                    iscollectfood=false;
                    Toast.makeText(context,"收藏菜品失败,获取的状态码为"+collectStatusBean.getSuccess(), Toast.LENGTH_SHORT).show();

                }
            }
            @Override
            public void onFail(String msg) {
                iscollectfood=false;
                Toast.makeText(context,"收藏菜品失败："+msg, Toast.LENGTH_SHORT).show();

            }
        });
        return iscollectfood;
    }
    private boolean collectShop(int shop_id) {
         BaseModel<CollectStatusBean> model=new BaseModel<>();
         model.callEnqueue(model.service.doCollectShop(userId,shop_id), new BaseListener<CollectStatusBean>() {
             @Override
             public void onResponse(CollectStatusBean collectStatusBean) {
                 if(collectStatusBean.getSuccess().equals("1")){
                     iscollectshop=true;
                 }
                 else {
                     Toast.makeText(context,"收藏店铺失败,获取的状态码为:"+collectStatusBean.getSuccess(), Toast.LENGTH_SHORT).show();
                     iscollectshop=false;
                 }
             }
             @Override
             public void onFail(String msg) {
                 Toast.makeText(context,"收藏店铺失败:"+msg, Toast.LENGTH_SHORT).show();
                 iscollectshop=false;
             }
         });
        return  iscollectshop;
    }
    public  void docollect(int userId, int shop_food_id, final int status){
        BaseModel<IsCollectBean> isCollectBeanBaseModel=new BaseModel<>();
        isCollectBeanBaseModel.callEnqueue(isCollectBeanBaseModel.service.isCollected(userId,shop_food_id,status), new BaseListener<IsCollectBean>() {
            @Override
            public void onResponse(IsCollectBean isCollectBean) {

                if(isCollectBean.getCollected().equals("0")){//0未收藏
                    if(status==0){
                        iscollectshop=false;
                    }else if(status==1){
                        iscollectfood=false;
                    }
                    setVisibility(nocollect_img,0);
                    setVisibility(collect_pic,2);
                }
                else if(isCollectBean.getCollected().equals("1")){
                    if(status==0){
                        iscollectshop=true;
                    }else if(status==1){
                        iscollectfood=true;
                    }
                    setVisibility(collect_pic,0);
                    setVisibility(nocollect_img,2);
                }
            }

            @Override
            public void onFail(String msg) {
                setVisibility(collect_pic,2);
                setVisibility(nocollect_img,2);
            }
        });

    }
}
