package com.example.hyf.orderdinner.bean;

public class CollectStatusBean {

    /**
     * success : 1
     */

    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
