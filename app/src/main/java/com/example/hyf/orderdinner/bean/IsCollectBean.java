package com.example.hyf.orderdinner.bean;

public class IsCollectBean {

    /**
     * collected : 0
     */

    private String collected;

    public String getCollected() {
        return collected;
    }

    public void setCollected(String collected) {
        this.collected = collected;
    }
}
