package com.example.hyf.orderdinner.bean;

public class MyShopCarbean {

    /**
     * item_id : 29
     * order_id : 0
     * food_id : 10
     * shop_id : 3
     * shopname : null
     * foodname : 酸菜丸子
     * num : 1
     * content : null
     * comment_time : null
     * pic : /images/food/酸菜丸子.jpg
     * price : 23.0
     */

    private int item_id;
    private int order_id;
    private int food_id;
    private int shop_id;
    private Object shopname;
    private String foodname;
    private int num;
    private Object content;
    private Object comment_time;
    private String pic;
    private double price;

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getFood_id() {
        return food_id;
    }

    public void setFood_id(int food_id) {
        this.food_id = food_id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public Object getShopname() {
        return shopname;
    }

    public void setShopname(Object shopname) {
        this.shopname = shopname;
    }

    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public Object getComment_time() {
        return comment_time;
    }

    public void setComment_time(Object comment_time) {
        this.comment_time = comment_time;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
