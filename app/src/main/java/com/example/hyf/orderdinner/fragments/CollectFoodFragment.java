package com.example.hyf.orderdinner.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.activity.FoodInfoActivity;
import com.example.hyf.orderdinner.adapter.CollectFoodAdapter;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.CollectListBean;

import java.util.List;

public class CollectFoodFragment extends Fragment {
    RecyclerView recyclerView;
    CollectFoodAdapter adapter;
    List<CollectListBean> listbeans;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_collect_food_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
        initEvent();
    }

    private void initEvent() {
        adapter.setOnItemClickListener(new CollectFoodAdapter.OnItemClickListener() {
            @Override
            public void setItem(View view, int position) {
                Intent intent=new Intent(getContext(),FoodInfoActivity.class);
                intent.putExtra("food_id",listbeans.get(position).getFood_id());
                startActivity(intent);
            }
        });
    }

    private void initData() {
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("OrderDinner",0);
        int user_id=sharedPreferences.getInt("USERID",0);
        BaseModel<List<CollectListBean>> model=new BaseModel<>();
        model.callEnqueue(model.service.getCollectList(user_id,1), new BaseListener<List<CollectListBean>>() {
            @Override
            public void onResponse(List<CollectListBean> collectListBeans) {
                listbeans=collectListBeans;
                adapter.setData(listbeans);
            }

            @Override
            public void onFail(String msg) {

            }
        });
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.rv_collectfood);
        adapter=new CollectFoodAdapter();
        recyclerView.setAdapter(adapter);
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
    }
}
