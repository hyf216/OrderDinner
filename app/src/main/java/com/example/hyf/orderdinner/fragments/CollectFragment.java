package com.example.hyf.orderdinner.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollectFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    ArrayList<String> titleDatas=new ArrayList<>();
    final List<Fragment> fragments=new ArrayList<>();
    public CollectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_collect, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        tabLayout=view.findViewById(R.id.tablayout);
//        tabLayout.addTab(tabLayout.newTab().setText("店铺"));
//        tabLayout.addTab(tabLayout.newTab().setText("菜品"));
        viewPager=view.findViewById(R.id.collect_viewpage);

        fragments.add(new CollectShopFragment());
        fragments.add(new CollectFoodFragment());
        titleDatas.add("店铺");
        titleDatas.add("菜品");

        viewPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
            @Override
            public CharSequence getPageTitle(int position) {

                      return titleDatas.get(position);
            }
        });

        tabLayout.setupWithViewPager(viewPager);
    }
}
