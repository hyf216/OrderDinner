package com.example.hyf.orderdinner.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.activity.ShopFoodActivity;
import com.example.hyf.orderdinner.adapter.CollectShopAdapter;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.CollectListBean;

import java.sql.Time;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class CollectShopFragment extends Fragment {
    private RecyclerView recyclerView;
    private CollectShopAdapter adapter;
    List<CollectListBean> listBeans;
    public CollectShopFragment(){}
    private boolean run = false;
    private final Handler handler = new Handler();
    int user_id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_collect_shop_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        initData();
//        run = true;
//        handler.postDelayed(task, 1000);
        initEvent();

    }

//        private final Runnable task = new Runnable() {
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                if (run) {
//                    initData();
//                    handler.postDelayed(this, 1000);
//                }
//            }
//        };

    private void initEvent() {
        adapter.setOnItemClickListener(new CollectShopAdapter.OnItemClickListener() {
            @Override
            public void setItem(View view, int position) {
                Intent intent=new Intent(getContext(),ShopFoodActivity.class);
                intent.putExtra("shop_id",listBeans.get(position).getShop_id());
                startActivity(intent);
            }
        });
    }

    private void initData() {
        BaseModel<List<CollectListBean>> model=new BaseModel<>();
        model.callEnqueue(model.service.getCollectList(Integer.valueOf(user_id),0), new BaseListener<List<CollectListBean>>() {
            @Override
            public void onResponse(List<CollectListBean> collectShopDatatBeans) {
                if(collectShopDatatBeans ==null){
                    Toast.makeText(getContext(),"CollectShopFrangment:"+"接收信息为空", Toast.LENGTH_SHORT).show();
                }else {
                    listBeans = collectShopDatatBeans;
                    adapter.setData(listBeans);
                }

            }

            @Override
            public void onFail(String msg) {
                Toast.makeText(getContext(), "CollectShopFragment:"+msg, Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void initView(View view) {
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_collectshop);
        adapter=new CollectShopAdapter();
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences("OrderDinner",0);
        user_id=sharedPreferences.getInt("USERID",0);
    }
}
