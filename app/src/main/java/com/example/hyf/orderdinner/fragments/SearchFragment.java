package com.example.hyf.orderdinner.fragments;



import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.activity.SearchRusltActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    EditText et_search_info;
    Button bt_search_confirm;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
        initEvent();
    }

    private void initEvent() {
//        adapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
//            @Override
//            public void setItem(View view, int position) {
//                Intent intent=new Intent(getContext(),FoodInfoActivity.class);
//                b.putIntegerArrayList("foodid",getfoodid);
//                intent.putExtras(b);
//                startActivity(intent);
//            }
//        });
    }

    private void initData() {
            bt_search_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   String search_info=et_search_info.getText().toString();
                    Intent intent=new Intent(getContext(),SearchRusltActivity.class);
                    intent.putExtra("search_info",search_info);
                    startActivity(intent);

//                    BaseModel<List<SearchResultBean>> model=new BaseModel<>();
//                    model.callEnqueue(model.service.getSearch(search_info), new BaseListener<List<SearchResultBean>>() {
//                        @Override
//                        public void onResponse(List<SearchResultBean> searchResultBeans) {
//                              if(searchResultBeans!=null){
//                                  adapter.setData(searchResultBeans);
////                                  for(int i=0;i<searchResultBeans.size();i++){
////                                      getfoodid.add(searchResultBeans.get(i).getFood_id());
////                                  }
//
//                              }else {
//                                  Toast.makeText(getContext(), "Response为空", Toast.LENGTH_SHORT).show();
//                              }
//                        }
//
//                        @Override
//                        public void onFail(String msg) {
//                            Toast.makeText(getContext(), "SearchFragment:"+msg, Toast.LENGTH_SHORT).show();
//                        }
//                    });
                }
            });
    }

    private void initView(View view) {
        et_search_info=(EditText)view.findViewById(R.id.et_search_info);
        bt_search_confirm=(Button)view.findViewById(R.id.bt_search_confirm);
//        recyclerView=(RecyclerView)view.findViewById(R.id.rv_search);
//        adapter=new SearchAdapter();
//        recyclerView.setAdapter(adapter);
//        LinearLayoutManager manager=new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
    }

}
