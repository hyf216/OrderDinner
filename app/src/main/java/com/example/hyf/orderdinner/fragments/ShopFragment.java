package com.example.hyf.orderdinner.fragments;



import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hyf.orderdinner.activity.ShopFoodActivity;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.adapter.AllShopAdapter;
import com.example.hyf.orderdinner.base.TopBar;
import com.example.hyf.orderdinner.bean.AllShopBean;
import com.example.hyf.orderdinner.model.AllShopModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends Fragment {
    private RecyclerView recyclerView;
    private AllShopAdapter allShopAdapter;
    private List<AllShopBean> allShopBeanList;
    SharedPreferences sp;
    public ShopFragment(){

    }

    BaseListener<List<AllShopBean>> listener = new BaseListener<List<AllShopBean>>() {
        @Override
        public void onResponse(List<AllShopBean> allShopBeans) {
            allShopBeanList=allShopBeans;
            if(allShopBeanList==null)
                Toast.makeText(getContext(),"接受信息为空",Toast.LENGTH_SHORT).show();
            else {
//                Toast.makeText(getContext(),allShopBeanList.get(0).getShopname(),Toast.LENGTH_SHORT).show();
                allShopAdapter.setData(allShopBeanList);
            }
        }
        @Override
        public void onFail(String fstr) {
            Toast.makeText(getContext(),"系统出错",Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_shop,container,false);

        return view;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //网络请求
        AllShopModel model=new AllShopModel();
        model.getAllShop(listener);

        initview(view);
    }

    private void initview(View view) {
        //adapter
        allShopAdapter=new AllShopAdapter();
        recyclerView=(RecyclerView)view.findViewById(R.id.shop_recylerview);
        recyclerView.setAdapter(allShopAdapter);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        //itemOnclick


       allShopAdapter.setOnItemClickListener(new AllShopAdapter.OnItemClickListener() {
           @Override
           public void setItem(View view, int position) {
               Intent intent = new Intent(getContext(),ShopFoodActivity.class);
               Integer msg=allShopBeanList.get(position).getShop_id();
               intent.putExtra("shop_id",msg);
               sp=getContext().getSharedPreferences("OrderDinner",0);
               SharedPreferences.Editor editor=sp.edit();
               editor.putInt("shop_id",allShopBeanList.get(position).getShop_id()).putInt("status",0).commit();
               startActivity(intent);

           }
       });
    }



}
