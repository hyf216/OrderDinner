package com.example.hyf.orderdinner.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyf.orderdinner.R;
import com.example.hyf.orderdinner.activity.LoginActivity;
import com.example.hyf.orderdinner.activity.ShoppingCarActivity;
import com.example.hyf.orderdinner.activity.UpdateInfoActivity;
import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.base.OnMainListener;
import com.example.hyf.orderdinner.base.URL;
import com.example.hyf.orderdinner.bean.UserInfoBean;
import com.squareup.picasso.Picasso;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserInfoFragment extends Fragment  {

    ImageView my_pic;
    TextView my_name,my_phonenum,myshopcar;
    Button bt_bianji;
    int userid;
    SharedPreferences sps;
    Timer timer;
    TimerTask timerTask;
    Handler handler;
//    OnMainListener onMainListener;
//    UpdateInfoActivity updateInfoActivity;
    public UserInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        flusData();
        timer.schedule(timerTask,0,500);
        initEvent();
    }

    public void flusData() {
       handler=new Handler(){
            public void handleMessage(Message msg) {
                if (msg.what == 1){
                    initData();
                }
                super.handleMessage(msg);
            }
        };
        timer=new Timer();
        timerTask=new TimerTask() {
            @Override
            public void run() {
                Message message=new Message();
                message.what=1;
                handler.sendMessage(message);
            }
        };
    }


    private void initEvent() {
        myshopcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),ShoppingCarActivity.class);
                startActivity(intent);
            }
        });
        bt_bianji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),UpdateInfoActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initData() {
        BaseModel<UserInfoBean> model=new BaseModel<>();
        model.callEnqueue(model.service.getUserInfo(userid), new BaseListener<UserInfoBean>() {
            @Override
            public void onResponse(UserInfoBean userInfoBean) {
                my_name.setText("\u3000\u3000"+userInfoBean.getUsername());
                my_phonenum.setText("\u3000\u3000"+userInfoBean.getMobilenum());
//                Picasso.get().load(URL.BaseURL+userInfoBean.getImg()).into(my_pic);
            }
            @Override
            public void onFail(String msg) {
//                Toast.makeText(getContext(), "UserInfoFragment:"+msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        my_pic=(ImageView)view.findViewById(R.id.my_pic);
        my_name=(TextView)view.findViewById(R.id.my_name);
        my_phonenum=(TextView)view.findViewById(R.id.my_phonenum);
        myshopcar=(TextView)view.findViewById(R.id.tv_myshopcar);
        bt_bianji=(Button)view.findViewById(R.id.my_bianji);
        sps=getContext().getSharedPreferences("OrderDinner",0);
        userid=sps.getInt("USERID",00000000000);
        SharedPreferences.Editor editor=sps.edit();
        editor.putInt("status",2).commit();

//        Toast.makeText(getContext(), "UserInfoFragment:"+userid, Toast.LENGTH_SHORT).show();
    }



}

