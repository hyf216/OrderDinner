package com.example.hyf.orderdinner.model;

import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.AllShopBean;

import java.util.List;

public class AllShopModel extends BaseModel<List<AllShopBean>> {
    public void getAllShop(final BaseListener baseListener){
           call = service.getAllShop();
           callEnqueue(call,baseListener);
    }
}
