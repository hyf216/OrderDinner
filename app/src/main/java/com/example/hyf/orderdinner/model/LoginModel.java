package com.example.hyf.orderdinner.model;

import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.ResultBean;

public class LoginModel extends BaseModel<ResultBean>{
    public void getlogin(String username, String userpass,final BaseListener listener){
        call=service.getlogin(username,userpass);
        callEnqueue(call,listener);
    }
}
