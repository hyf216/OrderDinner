package com.example.hyf.orderdinner.model;

import com.example.hyf.orderdinner.base.BaseListener;
import com.example.hyf.orderdinner.base.BaseModel;
import com.example.hyf.orderdinner.bean.RegisterBean;

public class RegistModel extends BaseModel<RegisterBean> {
    public void register(String username, String userpass, String mobilenum, String address,String comment, final BaseListener listener){
        call=service.register(username,userpass,mobilenum,address,comment);
        callEnqueue(call,listener);
    }
}
